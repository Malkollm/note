package ru.study.note.domain;

/***
 *  file: Message.java
 *
 *     @JsonView(Views.Id.class)
 *     private Long id;
 *
 *     @JsonView(Views.IdName.class)
 *     private String text;
 *
 *     @JsonView(Views.FullMessage.class)
 *     private LocalDateTime creationDate;
 *
 *     Какие поля помечены Views те поля и будут отображаться при запросе
 *
 *     Пример (file: MessageController.java):
 *
 *     @JsonView(Views.FullMessage.class)
 *     public Message getOne(@PathVariable("id") Message message){
 *         return message;
 *     }
 *        вернется: id & creationDate так как FullMessage расширяется от Id без поля Name
 */
public final class Views {
    public interface Id {} //get only id

    public interface IdName extends Id {} //get only id && name

    public interface FullMessage extends Id {} //get all fields
}
