package ru.study.note.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.study.note.domain.Message;

public interface MessageRepo extends JpaRepository<Message, Long> {
}
