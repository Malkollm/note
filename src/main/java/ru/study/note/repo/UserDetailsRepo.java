package ru.study.note.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.study.note.domain.User;

public interface UserDetailsRepo extends JpaRepository<User, String> {
}
